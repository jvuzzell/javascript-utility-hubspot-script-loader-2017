hubspotJsLoader = (function(){
  var library = {}, HTMLcollectionOfModules, registrationCount = 0, registrationInit = false;
  
  // Manage available scripts
  function registrationComplete( num ) {
    registrationCount = registrationCount + num; 
    if( registrationCount == 0 && registrationInit ){
      initModules();
    }
  } 
  
  // Find modules on page
  function detectModules() {
    HTMLcollectionOfModules = document.querySelectorAll( '[data-module]' );
    includeModuleScripts(); 
  }
  
  // Find module scripts
  function cwd() {
    var dir = document.querySelector( '[src*=hubspotJsLoader]' ).attributes['src'].value.replace( 'hubspotJsLoader.js', '' );

    return dir;
  }
  
  function modulePaths( list ) {
    var names = [], 
        paths = [],
        scriptsDir = cwd();
    
    for( var i=0; i < list.length; i++ ){
      name = list[i].dataset['module']; 
      
      if ( names.indexOf( name ) == -1 ) {
        names.push( name );
      }
    }
    
    // Build path
    for( var n=0; n < names.length; n++ ){
      paths.push( scriptsDir + names[n] + '.js');
    }
    
    return paths;
  } 
  
  function includeModuleScripts() {
    var paths = modulePaths( HTMLcollectionOfModules );
    
    for( var i=0; i < paths.length; i++ ){
      var imported = document.createElement( 'script' );
      imported.src = paths[i];
      document.head.appendChild( imported );  
      
      registrationComplete( -1 );
    }
  }
     
  // Store modules scripts in a library
  function registerModules( name , m  ) { 
    this.module = m;
    if( typeof( this.module ) == "function" ) {
      
      // Prevent duplicates
      if ( !library.hasOwnProperty( name ) ) {
        library[name] = this.module;	
      }      
    } else {
      console.error( "Module provided is not a function." );
    }
    
    registrationInit = true; 
    registrationComplete( 1 );
  }
  
  // Run scripts for each module on page
  function initModules() {
    HTMLcollectionOfModules.forEach( function( elem ) {
      moduleName = elem.dataset['module'];
      
      library[moduleName]( elem );
    }); 
  }

  return { 
    init     : detectModules,
    register : registerModules
  };

})();